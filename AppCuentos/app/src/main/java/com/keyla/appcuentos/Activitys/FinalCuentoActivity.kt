package com.keyla.appcuentos.Activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.keyla.appcuentos.R
import kotlinx.android.synthetic.main.activity_cuento4.*
import kotlinx.android.synthetic.main.activity_final_cuento.*

class FinalCuentoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final_cuento)

       val textLector = findViewById<TextView>(R.id.lector)

        val bundle: Bundle? = intent.extras
        bundle?.let {

            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")
            val cuento = it.getString("dato3")
            // txtLector.text = "$nombre estas leyendo: $cuento "
            textLector.text = "$nombre"

            buttonRegresar.setOnClickListener {
                val intent = Intent(this,MenuActivity::class.java)
                intent.putExtra("dato1",nombre)
                intent.putExtra("dato2",apellido)
                startActivity(intent)
                //startActivity(Intent(this, MenuActivity::class.java))

            }

        }


    }
}
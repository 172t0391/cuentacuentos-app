package com.keyla.appcuentos.Activitys

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.airbnb.lottie.LottieAnimationView
import com.keyla.appcuentos.R
import kotlinx.android.synthetic.main.activity_main.*

class RegistroActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        btnIngresar.setOnClickListener {

            val nombre = txtNombre.text.toString()
           val apellido = txtApellido.text.toString()
            val edad = txtEdad.text.toString()
            val contra1 = txtPassword.text.toString()
            val contra2 = txtPassword.text.toString()



            if (nombre.isEmpty()) {
                Toast.makeText(this, "Debe de ingresar un nombre", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (apellido.isEmpty()) {
            Toast.makeText(this, "Debe de ingresar una apellido", Toast.LENGTH_SHORT).show()
            return@setOnClickListener
        }

            if (edad.isEmpty()) {
                Toast.makeText(this, "Debe de ingresar una Edad", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (contra1.isEmpty()) {
                Toast.makeText(this, "Debe de ingresar un password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(contra2.isEmpty()){
                Toast.makeText(this, "Debe de ingresar la confirmacion de password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val intent = Intent(this,MenuActivity::class.java)

            intent.putExtra("dato1",nombre)
            intent.putExtra("dato2",apellido)
            intent.putExtra("dato3",edad)
            it.hideKeyboard()
            startActivity(intent)
            finish()


        }
        }
    fun View.hideKeyboard(){
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    }





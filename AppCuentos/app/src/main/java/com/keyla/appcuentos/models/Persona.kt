package com.keyla.appcuentos.models

data class Persona(val nombre: String, val apellido:String, val edad: Int)
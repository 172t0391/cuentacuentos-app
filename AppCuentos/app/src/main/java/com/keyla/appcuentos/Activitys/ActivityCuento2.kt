package com.keyla.appcuentos.Activitys

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.viewpager.widget.ViewPager
import com.airbnb.lottie.LottieAnimationView
import com.keyla.appcuentos.R
import kotlinx.android.synthetic.main.activity_activiy_cuento_parte2.*
import kotlinx.android.synthetic.main.activity_cuento.*
import kotlinx.android.synthetic.main.activity_cuento.txtLector
import kotlinx.android.synthetic.main.dialog_pregunta.*

class ActivityCuento2 : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_activiy_cuento_parte2)

        val btnContinuar = findViewById<Button>(R.id.btnContinuar)

        val bundle: Bundle? = intent.extras
        bundle?.let {

            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")
            val cuento = it.getString("dato3")
            txtLector.text = "$nombre estas leyendo: $cuento "

            btnContinuar.setOnClickListener { goToCuentoParte3(nombre,apellido) }

        }

    }

    private fun goToCuentoParte3(nombre : String?, apellido : String?){

        val intent = Intent(this,CuentoActivity3::class.java)
        intent.putExtra("dato1",nombre)
        intent.putExtra("dato2",apellido)
        startActivity(intent)
    }

}

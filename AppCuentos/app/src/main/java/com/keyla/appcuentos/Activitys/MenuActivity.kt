package com.keyla.appcuentos.Activitys

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.keyla.appcuentos.R
import com.keyla.appcuentos.models.Persona

class MenuActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val btnCuento = findViewById<Button>(R.id.btnCuento)
        val txtNombre = findViewById<TextView>(R.id.txtNombre)
        val btnCerditos = findViewById<Button>(R.id.btnCerditos)
        val bundle: Bundle? = intent.extras
        bundle?.let {

            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")
            //val edad = it.getString("dato3")
            txtNombre.text = "$nombre $apellido"

           btnCuento.setOnClickListener { goToCuento(nombre,apellido) }

           btnCerditos.setOnClickListener { goToCerditos(nombre,apellido) }
        }
    }
    private fun goToCuento(nombre : String?, apellido : String?){
        val intent = Intent(this,PatitoActivity::class.java)
        intent.putExtra("dato1",nombre)
        intent.putExtra("dato2",apellido)
        startActivity(intent)


    }
    private fun goToCerditos(nombre : String?, apellido : String?){
        val intent = Intent(this,CerditoActivity::class.java)
        intent.putExtra("dato1",nombre)
        intent.putExtra("dato2",apellido)
        startActivity(intent)

    }






}